package com.example.berlianjaya;

import androidx.appcompat.app.AppCompatActivity;

import androidx.annotation.Nullable;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    Login activity;
    EditText username,password;
    private ProgressDialog dialog;
    private Button login_button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.activity =this;
        username = findViewById(R.id.input_username);
        password = findViewById(R.id.input_password);

        dialog = new ProgressDialog(activity);

        login_button =findViewById(R.id.login_button);
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username.getText().toString().equals("") || password.getText().toString().equals("")) {
                    Toast.makeText(activity, "Data tidak boleh kosong", Toast.LENGTH_LONG).show();
                } else{
                    Log.e("CheckUSN", username.getText().toString());
                    Log.e("CheckPSW", password.getText().toString());
                    runvolleypost();
                }
            }
        });
    }

    void runvolleypost() {
        dialog.setMessage("Harap Tungu..!");
        dialog.setCancelable(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, (new GenerateStr()).API("login"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            JSONObject xx = new JSONObject(response);
                            Log.e("dd", xx.toString(3));//test semua response
                            Log.e("dd", xx.getString("status"));

                            String xstatus = xx.getString("status");
                            String xpesan = xx.getString("message");
                            Log.e("dd", xstatus);

                            if (xx.getString("status").equals("success")) {
                                String test = "MASUK";
                                Log.e("cek", test);
                                // isi token ke session
                                SharedPreferences sp = activity.getSharedPreferences("shared", 0x0000);
                                SharedPreferences.Editor editorr = sp.edit();
                                editorr.putString("token", xx.getString("token"));
                                editorr.putString("user", xx.getString("user"));
                                editorr.putString("role", xx.getString("role"));
                                editorr.apply();
                                //pindah ke menu nih karena berhasil
                                Intent pindah = new Intent(Login.this, MainActivity.class);
                                startActivity(pindah);
                                finish();
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            } else {
                                String test = "KELUAR";
                                Log.e("dd", test);
                                new AlertDialog
                                        .Builder(activity, R.style.AlertDialogCustom)
                                        .setCancelable(false).setTitle("Login Gagal")
                                        .setMessage(xpesan)
                                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Toast.makeText(activity, error.getMessage(), Toast.LENGTH_LONG).show();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                } catch (Exception se) {

                }

            }
        }) {

            //buat kirim data
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> postMap = new HashMap<>();
                postMap.put("user", username.getText().toString().trim());
                postMap.put("password", password.getText().toString().trim());
                postMap.put("type", "mobile");
                return postMap;
            }

        };
        //make the request to your server as indicated in your request url
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(activity).add(stringRequest);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


}