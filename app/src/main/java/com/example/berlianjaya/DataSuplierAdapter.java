package com.example.berlianjaya;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.berlianjaya.Model.MSuplier;

import java.util.List;

//import com.google.gson.JsonObject;

public class DataSuplierAdapter extends RecyclerView.Adapter<DataSuplierAdapter.MyViewHolder>{
    private List<MSuplier> nilsemlist;
    private Context context;
    //    private GenKey key;
    private SharedPreferences sp;




    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView kd_suplier, nama_suplier, nomor_tlp, alamat_suplier;

        public MyViewHolder(View view) {
            super(view);

            kd_suplier = (TextView) view.findViewById(R.id.kd_suplier);
            nama_suplier =(TextView) view.findViewById(R.id.nama_suplier);
            nomor_tlp =(TextView) view.findViewById(R.id.nomor_tlp);
            alamat_suplier =(TextView) view.findViewById(R.id.alamat_suplier);


        }
    }

    public DataSuplierAdapter(List<MSuplier> nilsemlist, Context context, SharedPreferences sp) {
        this.nilsemlist = nilsemlist;
        this.context = context;
//        this.key = key;
        this.sp = sp;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.suplier_row, parent, false);


        return new MyViewHolder(itemView);

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final MSuplier nilsem = nilsemlist.get(position);
        holder.kd_suplier.setText(nilsem.getStr_kd_suplier());
        holder.nama_suplier.setText(nilsem.getStr_nama_suplier());
        holder.nomor_tlp.setText(nilsem.getStr_nomor_tlp());
        holder.alamat_suplier.setText(nilsem.getStr_alamat_suplier());
    }

    @Override
    public int getItemCount() {
        return nilsemlist.size();
    }


}
