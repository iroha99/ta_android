package com.example.berlianjaya;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.berlianjaya.Model.MBarang;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.google.gson.JsonObject;

public class DataBarang extends AppCompatActivity{
    private RecyclerView recyclerView;
    private DataBarangAdapter mAdapter;
    private JSONArray pel;
    private JSONObject a;
    //    private GenKey key;
    private SharedPreferences sp;
    private DataBarang activity;
    private Integer x;
    private List<MBarang> nilsemlist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_barang);

//        key = new GenKey();
        this.activity = this;
//        sp = activity.getSharedPreferences("shared", 0x0000);
        Button back = findViewById(R.id.back);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        recyclerView = findViewById(R.id.rv_barang);

        mAdapter = new DataBarangAdapter(nilsemlist, activity, sp);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        dash();

    }

    void dash() {
//        key.showProgress(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, (new GenerateStr()).API("barang"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        key.hideProgress();
                        try {
//                            JSONObject json = new JSONObject(response);
                            JSONArray json = new JSONArray(response);
//                            JSONArray json = new JSONArray(response);
//                            String code = json.getString("code");
//                            Log.e("KEK", json.toString(3));
//                            String xstatus = json.getString("status");
//                            if (code.equals("200")) {

                            String  Str_kd_barang, Str_nama_barang, Str_kd_kategori, Str_harga_jual, Str_stok;
                            MBarang hadir;
//                                pel = json.getJSONArray("data_barang");
                            for (x = 0; x < json.length(); x++) {
//                                    a = pel.getJSONObject(x);
                                JSONObject a = json.getJSONObject(x);
                                Log.e("KEK", a.getString("nama_barang"));
//                                    String Str_kd_barang, String Str_nama_barang, Integer Int_harga_jual,String Str_kd_kategori,Integer Str_stok
                                Str_kd_barang = a.getString("kd_barang");
                                Str_nama_barang = a.getString("nama_barang");
                                Str_harga_jual = a.getString("harga_jual");
                                Str_kd_kategori = a.getString("kd_kategori");
//                                    Str_kd_supplier = a.getString("kd_supplier");
                                Str_stok = a.getString("stok");

//                                    String Str_kd_barang, String Str_nama_barang, String Str_harga_jual,String Str_kd_kategori,String Str_stok
                                hadir = new MBarang(Str_kd_barang, Str_nama_barang, Str_harga_jual, Str_kd_kategori, Str_stok);
                                nilsemlist.add(hadir);

                            }


                            mAdapter.notifyDataSetChanged();



//                            } else {
//                                androidx.appcompat.app.AlertDialog.Builder ab = new AlertDialog.Builder(activity);
//                                ab.setCancelable(false).setTitle("Informasi");
//                                ab.setMessage(json.getString("msg")).setPositiveButton("Tutup", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                    }
//                                }).show();
//                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

//                key.hideProgress();

                try {
                    String message;
                    if (!(error instanceof NetworkError | error instanceof TimeoutError)) {

                        NetworkResponse networkResponse = error.networkResponse;
                        message = "Gagal terhubung dengan server, siahkan coba beberapa menit lagi\nError Code: " + networkResponse.statusCode;

                    } else {
                        Log.e("ER", (error instanceof NetworkError) + "" + error.getMessage());
                        message = "Gagal terhubung dengan server, siahkan coba beberapa menit lagi";
                    }


                    new androidx.appcompat.app.AlertDialog.Builder(activity)
                            .setTitle("Informasi")
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Coba Lagi", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                }
                            }).show();
                } catch (Exception se) {
                    se.printStackTrace();
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> postMap2 = new HashMap<>();
                try {
//                    JsonObject xdata = new JsonObject();
//                    Log.e("er", xdata.toString());
//                    postMap2.put("kirimanpokoknya", xdata.toString());
                } catch (Exception e) {
                    postMap2 = null;
                }
                return postMap2;
            }
        };

        //make the request to your server as indicated in your request URL
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(activity).add(stringRequest);
    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }
}
