package com.example.berlianjaya;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.berlianjaya.Model.MBarang;

import java.util.List;

//import com.google.gson.JsonObject;

public class DataBarangAdapter extends RecyclerView.Adapter<DataBarangAdapter.MyViewHolder> {
    private List<MBarang> nilsemlist;
    private Context context;
    //    private GenKey key;
    private SharedPreferences sp;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView kd_barang, nama_barang, kd_kategori, stok;
        Button update;

        public MyViewHolder(View view) {
            super(view);

            kd_barang = (TextView) view.findViewById(R.id.kd_barang);
            nama_barang = (TextView) view.findViewById(R.id.nama_barang);
            kd_kategori = (TextView) view.findViewById(R.id.kd_kategori);
            stok = (TextView) view.findViewById(R.id.stok);
            update = (Button) view.findViewById(R.id.update);


        }
    }

    public DataBarangAdapter(List<MBarang> nilsemlist, Context context, SharedPreferences sp) {
        this.nilsemlist = nilsemlist;
        this.context = context;
//        this.key = key;
        this.sp = sp;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.barang_row, parent, false);


        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final MBarang nilsem = nilsemlist.get(position);
        holder.kd_barang.setText(nilsem.getStr_kd_barang());
        holder.nama_barang.setText(nilsem.getStr_nama_barang());
        holder.kd_kategori.setText(nilsem.getStr_kd_kategori());
        holder.stok.setText(nilsem.getStr_stok());
        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, Update.class);
                i.putExtra("kd_barang", nilsem.getStr_kd_barang());
                i.putExtra("nama_barang", nilsem.getStr_nama_barang());
                i.putExtra("kd_kategori", nilsem.getStr_kd_kategori());
                i.putExtra("stok", nilsem.getStr_stok());
                context.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return nilsemlist.size();
    }


}
