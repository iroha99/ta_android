package com.example.berlianjaya;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigInteger;

class GenerateStr {
    String API(String str) {
        String link = "192.168.100.39:8080";
        switch (str) {
            case "barang":
                return "http://" + link + "/api/barang";
            case "updatestok":
                return "http://" + link + "/api/barang/updateandro";
            case "detailbarang":
                return "http://" + link + "/api/detail/barang";
            case "kategori":
                return "http://" + link + "/api/kategori";
            case "suplier":
                return "http://" + link + "/api/suplier";
            case "login":
                return "http://" + link + "/api/login";
            default:
                return "http://" + link +"/";
        }
    }

    String dekripsi(JSONArray array) {
        try {
            JSONObject obj;

            int p = 2903, x = 1751;
            StringBuilder xx = new StringBuilder();
            for (int i = 0; i < array.length(); i++) {
                obj = array.getJSONObject(i);

                BigInteger y = BigInteger.valueOf(obj.getInt("y"));
                BigInteger g = BigInteger.valueOf(obj.getInt("g"));

                BigInteger m = g.multiply(y.pow(p - 1 - x)).mod(BigInteger.valueOf(p));
                int ma = m.intValue();
                xx.append((char) ma);
            }
            return xx.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }
}
