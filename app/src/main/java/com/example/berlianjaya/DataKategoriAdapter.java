package com.example.berlianjaya;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.berlianjaya.Model.MKategori;

import java.util.List;

//import com.google.gson.JsonObject;

public class DataKategoriAdapter extends RecyclerView.Adapter<DataKategoriAdapter.MyViewHolder>{
    private List<MKategori> nilsemlist;
    private Context context;
    //    private GenKey key;
    private SharedPreferences sp;




    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView kd_kategori, nama_kategori;

        public MyViewHolder(View view) {
            super(view);

            kd_kategori = (TextView) view.findViewById(R.id.kd_kategori);
            nama_kategori = (TextView) view.findViewById(R.id.nama_kategori);


        }
    }

    public DataKategoriAdapter(List<MKategori> nilsemlist, Context context, SharedPreferences sp) {
        this.nilsemlist = nilsemlist;
        this.context = context;
//        this.key = key;
        this.sp = sp;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.kategori_row, parent, false);


        return new MyViewHolder(itemView);

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final MKategori nilsem = nilsemlist.get(position);
        holder.kd_kategori.setText(nilsem.getStr_kd_kategori());
        holder.nama_kategori.setText(nilsem.getStr_nama_kategori());
    }

    @Override
    public int getItemCount() {
        return nilsemlist.size();
    }


}
