package com.example.berlianjaya.Model;

public class MBarang {
    private String Str_kd_barang;
    private String Str_nama_barang;
    private String Str_harga_jual;
    private String Str_kd_kategori;
    private String Str_stok;

    public MBarang(String Str_kd_barang, String Str_nama_barang, String Str_harga_jual,String Str_kd_kategori,String Str_stok) {
        this.Str_kd_barang = Str_kd_barang;
        this.Str_nama_barang = Str_nama_barang;
        this.Str_harga_jual = Str_harga_jual;
        this.Str_kd_kategori = Str_kd_kategori;
        this.Str_stok = Str_stok;
    }

//    private String Str_nohp;

    public String getStr_kd_barang() {
        return Str_kd_barang;
    }

    public String getStr_nama_barang() {
        return Str_nama_barang;
    }

    public String getStr_harga_jual() {
        return Str_harga_jual;
    }

    public String getStr_kd_kategori() {
        return Str_kd_kategori;
    }

    public String getStr_stok() {
        return Str_stok;
    }

}
