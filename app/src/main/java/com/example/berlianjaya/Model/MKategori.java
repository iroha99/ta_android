package com.example.berlianjaya.Model;

public class MKategori {
    private String Str_kd_kategori;
    private String Str_nama_kategori;

    public MKategori(String Str_kd_kategori, String Str_nama_kategori) {
        this.Str_kd_kategori= Str_kd_kategori;
        this.Str_nama_kategori = Str_nama_kategori;
    }

//    private String Str_nohp;

    public String getStr_kd_kategori() {
        return Str_kd_kategori;
    }

    public String getStr_nama_kategori() {
        return Str_nama_kategori;
    }

}
