package com.example.berlianjaya.Model;

public class MSuplier {
    private String Str_kd_suplier;
    private String Str_nama_suplier;
    private String Str_nomor_tlp;
    private String Str_alamat_suplier;

    public MSuplier(String Str_kd_suplier, String Str_nama_suplier, String Str_nomor_tlp,String Str_alamat_suplier) {
        this.Str_kd_suplier = Str_kd_suplier;
        this.Str_nama_suplier = Str_nama_suplier;
        this.Str_nomor_tlp = Str_nomor_tlp;
        this.Str_alamat_suplier = Str_alamat_suplier;
    }

//    private String Str_nohp;

    public String getStr_kd_suplier() {
        return Str_kd_suplier;
    }

    public String getStr_nama_suplier() {
        return Str_nama_suplier;
    }

    public String getStr_nomor_tlp() {
        return Str_nomor_tlp;
    }

    public String getStr_alamat_suplier() {
        return Str_alamat_suplier;
    }

}
