package com.example.berlianjaya;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Update extends AppCompatActivity {
    String stok;
    String kd_barang;
    EditText stok_input;
    Button update;
    Update activity;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        this.activity =this;
        Intent tangkap = getIntent();
        dialog = new ProgressDialog(activity);
        stok = tangkap.getStringExtra("stok");
        kd_barang = tangkap.getStringExtra("kd_barang");
        stok_input = findViewById(R.id.stok_input);
        update = findViewById(R.id.update);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(stok_input.getText().toString().equals("")) {
                    Toast.makeText(activity, "Data tidak boleh kosong", Toast.LENGTH_LONG).show();
                } else{
                    Log.e("stok", stok_input.getText().toString());
                    runvolleypost();
                }
            }
        });

    }

    void runvolleypost() {
        dialog.setMessage("Harap Tungu..!");
        dialog.setCancelable(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, (new GenerateStr()).API("updatestok") + "/" + kd_barang,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            JSONObject xx = new JSONObject(response);
                            Log.e("dd", xx.toString(3));//test semua response
                            Log.e("dd", xx.getString("status"));

                            String xstatus = xx.getString("status");
                            String xpesan = xx.getString("message");
                            Log.e("dd", xstatus);

                            if (xx.getString("status").equals("success")) {
                                String test = "MASUK";
                                Log.e("cek", test);
                                // isi token ke session
//                                SharedPreferences sp = activity.getSharedPreferences("shared", 0x0000);
//                                SharedPreferences.Editor editorr = sp.edit();
//                                editorr.putString("token", xx.getString("token"));
//                                editorr.putString("user", xx.getString("user"));
//                                editorr.putString("role", xx.getString("role"));
//                                editorr.apply();
                                //pindah ke menu nih karena berhasil
                                new AlertDialog
                                        .Builder(activity, R.style.AlertDialogCustom)
                                        .setCancelable(false).setTitle("Update Berhasil")
                                        .setMessage(xpesan)
                                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).show();
                                Intent pindah = new Intent(Update.this, MainActivity.class);
                                startActivity(pindah);
                                finish();
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            } else {
                                String test = "KELUAR";
                                Log.e("dd", test);
                                new AlertDialog
                                        .Builder(activity, R.style.AlertDialogCustom)
                                        .setCancelable(false).setTitle("Update Gagal")
                                        .setMessage(xpesan)
                                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    Toast.makeText(activity, error.getMessage(), Toast.LENGTH_LONG).show();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                } catch (Exception se) {

                }

            }
        }) {

            //buat kirim data
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> postMap = new HashMap<>();
                postMap.put("stok", stok_input.getText().toString().trim());
                postMap.put("kd_barang", kd_barang.toString().trim());
                return postMap;
            }

        };
        //make the request to your server as indicated in your request url
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(activity).add(stringRequest);
    }

    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}